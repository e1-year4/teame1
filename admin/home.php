<?php
//include auth_session.php file on all user panel pages
include("../auth_session.php");
include('../functions.php')
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Dashboard - Client area</title>
    <link rel="stylesheet" href="../assets/admin_style.css" />
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
</head>

<body>
    <div class="form">
        <p>Hey, <?php echo $_SESSION['username']; ?>!</p>
        <p><a href="/teame1/admin/dashboard.php">Go to select pages</a></p>
        <p><a href="logout.php">Logout</a></p>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8">

                <?php
                if (isset($_SESSION['status'])) {
                ?>
                    <div role="alert">
                        <strong>Hey!</strong> <?php echo $_SESSION['status']; ?>
                        <button type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php
                    unset($_SESSION['status']);
                }
                ?>

                <div>
                    <div>
                        <h4>How to Insert Date Values into Database in php</h4>
                    </div>
                    <div>

                        <form action="home.php" method="POST">
                            <div>
                                <label for="">Please enter Name</label>
                                <input type="text" name="name" />
                            </div>
                            <div>
                                <label for="">Please enter Date of Birth</label>
                                <input type="date" name="BirthDate" />
                            </div>
                            <div>
                                <button type="submit" name="home">store Data</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>