
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="author" content="Egprojets">
	<meta name="description" content="" />
	<title>ម្ហូបខ្មែរ</title>

	<!-- Contribute CSS Files -->
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" />

	<!-- Custom CSS Files -->
	<link rel="stylesheet" href="../assets/css/style.css" />
	<link rel="stylesheet" href="../assets/css/mystyle.css" />
	<link rel="stylesheet" href="../assets/css/responsive.css" />
	<link rel="stylesheet" href="../assets/css/responsive.css" />
	<link rel="stylesheet" href="../admin/assets/admin_style.css" />
    <link rel="stylesheet" href="../assets/css/admin.css" />

	<!-- font khmer  -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Battambang:wght@300;400&family=Poppins&display=swap" rel="stylesheet">

 	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"> -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

		</head>
<body>

	<!-- Site Wrapper -->
	<div id="site-wrapper">
		<!-- Header -->
		<header id="site-header">

			<div class="navbar" role="navigation">
				<div class="container">
					<div class="row">
						<h1 class="sr-only">Food Lover</h1>
						<a href="/teame1" title="FoodLover" class="logo">
							<img src="../assets/img/LOGO_Khfood.png" alt="" width="170">
						</a>
						<button data-target=".navbar-collapse" data-toggle="collapse" type="button" class="menu-mobile visible-xs">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<ul class="nav navbar-nav navbar-collapse collapse">
							<li><a class="menu active" href="/teame1/">ទំព័រដើរ</a></li>
							<li><a class="menu" href="about-us.php">អំពីយើង</a></li>
							<li><a class="menu" href="menu.php">បញ្ជីមុខម្ហូប</a></li>
							<li><a class="menu" href="blog.php">ប្លុកយើង</a></li>
							<li><a class="menu" href="contact-us.php">ទំនាក់ទំនង</a></li>
							<li><a class="menu" href="video.php">សកម្មភាពផ្សេងៗ</a></li>
						</ul>

					</div>
				</div>
			</div>
		</header>
		<!-- End Header -->