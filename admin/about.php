<?php
include_once 'db.php';
$result = mysqli_query($con,"SELECT * FROM about");
include 'header.php';
?>

<style>
    #site-header {
        position: relative;
    }
    .container {
        margin-bottom: 70px;
    }
</style>

<?php
if (mysqli_num_rows($result) > 0) {
?>
<table>
	  <tr>
	    <td>Sl No</td>
		<td>First Name</td>
		<td>Last Name</td>
		<td>City</td>
		<td>Email id</td>
		<td>Action</td>
	  </tr>
			<?php
			$i=0;
			while($row = mysqli_fetch_array($result)) {
			?>
	  <tr>
	    <td><?php echo $row["id"]; ?></td>
		<td><?php echo $row["about_us"]; ?></td>
		<td><?php echo $row["des_about"]; ?></td>
		<td><?php echo $row["about_img"]; ?></td>
		<td><?php echo $row["num_foods"]; ?></td>
		<td><a href="about_update.php?id=<?php echo $row["id"]; ?>">Update</a></td>
      </tr>
			<?php
			$i++;
			}
			?>
</table>
 <?php
}
else
{
    echo "No result found";
}
?>
 </body>
</html>