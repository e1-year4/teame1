<?php include('header.php');  ?>
	<!-- Site Wrapper -->
	<div id="site-wrapper">
		<!-- Header -->
		<header id="site-header">

			<div class="navbar" role="navigation">
				<div class="container">
					<div class="row">
						<h1 class="sr-only">Food Lover</h1>
						<a href="index.html" title="FoodLover" class="logo">
							<img src="assets/img/LOGO_Khfood.png" alt="" width="170">
						</a>
						<button data-target=".navbar-collapse" data-toggle="collapse" type="button"
							class="menu-mobile visible-xs">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<ul class="nav navbar-nav navbar-collapse collapse">
							<li><a class="menu " href="index.html">ទំព័រដើរ</a></li>
							<li><a class="menu active" href="about-us.html">អំពីពួកយើង</a></li>
							<li><a class="menu" href="menu.html">បញ្ជីមុខម្ហូប</a></li>
							<li><a class="menu" href="blog.html">ប្លុកយើង</a></li>
							<li><a class="menu" href="contact-us.html">ទំនាក់ទំនង</a></li>
							<li><a class="menu" href="contact-us.html">សកម្មភាពផ្សេងៗ</a></li>
						</ul>
					</div>
				</div>
			</div>
		</header>
		<!-- End Header -->


		<section id="breadcrumb" class="main_cover_page">
			<div>
				<h1 class="section-title white-font text-center">អំពីពួកយើង</h1>
				<ul>
					<li><a href="index.html">ទំព័រដើរ</a></li>
					<li>អំពីពួកយើង</li>
				</ul>
			</div>
		</section>
		<!-- <section class="main_cover_page">
			<div>
				<h1 class="section-title white-font text-center">អំពីពួកយើង</h1>
				<ul>
					<li><a href="index.html">ទំព័រដើរ</a></li>
					<li>អំពីពួកយើង</li>
				</ul>
			</div>
		</section> -->

		<!-- End Section Main -->

		<!-- Section Story -->
		<section id="our-story">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="story-description">
							<h2 class="section-title">គោលបំណងរបស់របស់ពួកយើង</h2>
							<p>
								ពួកយើងមកពី <b>សកលវិទ្យាល័យភូមិន្ទភ្នំពេញ​(ជំនាញព័ត៌មានវិទ្យា)</b>
								បានបង្កើតគេហទំព័រមួយនេះក្នុងគោលបំណងចង់ចែករំលែកអំពីរបៀបនៃការធ្វើ​ ម្ហូប បង្អែម​ ទឹកជ្រលក់
								ភេសជ្ជៈជាច្រើនប្រភេទបែបខ្មែរដែលអ្នកទាំងអស់គ្នាមិនទាន់ដឺងអំពីរបៀបធ្វើ
								ដើម្បីជួយសម្រូលទៅដល់អ្នកទាំងអស់គ្នាក្នុងការរស់នៅប្រចាំថ្ងៃ...។
							</p>
							<div class="row">
								<div class="col-md-12 col-sm-12 techno_lang">
									<h4>ក្រុមយើងប្រើ Language Technology ដូចជា</h4>
									<div class="lang_list">
										<ul>
											<li><i class="far fa-code"></i><a href="#">HTML and CSS</a></li>
 											<li><i class="far fa-code"></i><a href="https://getbootstrap.com/docs/3.3/css/" target="_blank" rel="noopener noreferrer">Bootstrap v3</a></li>
											<li><i class="far fa-code"></i><a href="https://www.npmjs.com/package/@bedrock-layout/masonry-grid" target="_blank" rel="noopener noreferrer">plugin javascript</a></li>
											<li><i class="far fa-code"></i><a href="https://fontawesome.com/" target="_blank" rel="noopener noreferrer">fontawesome v5 Js</a></li>
											<li><i class="far fa-code"></i><a href="https://gitlab.com/e1-year4/teame1.git" target="_blank" rel="noopener noreferrer">Gitlab</a></li>
										</ul>
			
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<img src="assets/img/demo/story/team.jpg" alt="" class="img-responsive img-story">
					</div>
				</div>

		

			</div>
		</section>
		<!-- End Section Story -->

		<!-- Section Vision -->
		<section id="vision" class="parallax-window padd-100">
			<h2 class="sr-only">Vision</h2>
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="vision-item">
							<span class="count">168</span>
							<p>មុខម្ហូប</p>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="vision-item">
							<span class="count">19</span>
							<p>លិខិតសរសើរ</p>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="vision-item">
							<span class="count">8</span>
							<p>សមាជិក</p>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="vision-item last-item">
							<span class="count">15</span>
							<p>លិខិតទទូលស្គាល់</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Section Vision -->

		<!-- Section Teams -->
		<section id="team" class="padd-100">
			<h4 class="section-title sep-type-2 text-center">
				សមាជិកចូលរូមបង្កើតគេហទំព័រ
			</h4>
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="our-team">
							<div class="pic">
								<img src="assets/img/demo/team/sokha.jpg">
							</div>
							<h3 class="title">Khin Sokha</h3>
							<span class="post">Web Designer</span>
							<ul class="social">
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
								<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>

					<div class="col-md-3 col-sm-6">
						<div class="our-team">
							<div class="pic">
								<img src="assets/img/demo/team/image2.jpg">
							</div>
							<h3 class="title">Horm Kosfianann</h3>
							<span class="post">Web Designer</span>
							<ul class="social">
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
								<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>

					<div class="col-md-3 col-sm-6">
						<div class="our-team">
							<div class="pic">
								<img src="assets/img/demo/team/image3.jpg">
							</div>
							<h3 class="title">Ghheav sinh</h3>
							<span class="post">Web Designer</span>
							<ul class="social">
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
								<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="our-team">
							<div class="pic">
								<img src="assets/img/demo/team/image4.jpg">
							</div>
							<h3 class="title">O TOLA</h3>
							<span class="post">Web Designer</span>
							<ul class="social">
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
								<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="our-team">
							<div class="pic">
								<img src="assets/img/demo/team/image5.jpg">
							</div>
							<h3 class="title">Ya Paly</h3>
							<span class="post">Web Developer</span>
							<ul class="social">
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
								<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>

					<div class="col-md-3 col-sm-6">
						<div class="our-team">
							<div class="pic">
								<img src="assets/img/demo/team/image6.jpg">
							</div>
							<h3 class="title">Hach Ratha</h3>
							<span class="post">Photo Designer</span>
							<ul class="social">
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
								<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>

					<div class="col-md-3 col-sm-6">
						<div class="our-team">
							<div class="pic">
								<img src="assets/img/demo/team/image7.jpg">
							</div>
							<h3 class="title">Noeun Koemhuoy</h3>
							<span class="post">Web Designer</span>
							<ul class="social">
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
								<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="our-team">
							<div class="pic">
								<img src="assets/img/demo/team/image8.jpg">
							</div>
							<h3 class="title">Yun Sokunthyreach</h3>
							<span class="post">Web Designer</span>
							<ul class="social">
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
								<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</section>
		<!-- Footer -->
	

<?php include('footer.php'); ?>