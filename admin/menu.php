<?php
include("db.php");
include("auth_session.php");
 
if (isset($_POST['submit']) && !empty($_FILES["img_foods"]["name"])) {
    @$file = $_FILES['img_foods'];
    // print_r($file);
    @$fileName = ($_FILES['img_foods']['name']);
    @$fileTmpName = $_FILES['img_foods']['tmp_name'];
    @$fileSize = $_FILES['img_foods']['size'];
    @$fileError = $_FILES['img_foods']['error'];
    @$fileType = $_FILES['img_foods']['type'];

    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));

    $allowed = array('jpg', 'jpeg', 'png');

    if (in_array($fileActualExt, $allowed)) {
        if ($fileError === 0) {
            if ($fileSize < 1000000) {
                $fileNameNew = uniqid('', true) . "." . $fileActualExt;
                $fileDestination = 'img_foods/' . $fileNameNew;
                move_uploaded_file($fileTmpName, $fileDestination);
                // Insert image file name into database
                header("Location: create_menu.php?uplaodsuccess!");
            } else {
                echo "Your file is too big!";
            }
        } else {
            echo "There was an error uploading your file !";
        }
        #code ..

    } else {
        echo "You cannot upload files of this type";
    }
}
