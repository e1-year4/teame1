<?php
// include("db.php");
require_once 'db.php';
require_once 'crudmenu.php';

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Dashboard - Client area</title>
    <link rel="stylesheet" href="../admin/assets/admin_style.css" />
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
</head>

<body>
    <div class="container">
        <div class="form">
            <p>Creat Menu foods</p>
            <p><a href="/teame1/admin/dashboard.php">Go to select pages</a></p>
            <p><a href="logout.php">Logout</a></p>
        </div>

        <form action="menu_operation.php" method="POST" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="namefood">Name Of Food</label>
                        <input type="text" name="namefood" id="namefood" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="pfood">Price</label>
                        <input type="munber" step="any" name="pfood" id="pfood" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="typefoods" id="lunch" value="lunch" checked>
                                <label class="form-check-label" for="lunch">
                                    អាហារថ្ងៃត្រង់
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="typefoods" id="dinner" value="dinner" required>
                                <label class="form-check-label" for="dinner">
                                    អាហារពេលល្ងាច
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="typefoods" id="drink" value="drink" required>
                                <label class="form-check-label" for="drink">
                                    ភេសជ្ជៈ
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="typefoods" id="dessert" value="dessert" required>
                                <label class="form-check-label" for="dessert">
                                    បង្អែមខ្មែរនិងបរទេស
                                </label>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="custom-file">
                        <label class="custom-file-lable" for="imgfoods">Foods picture</label>
                        <input type="file" accept="image/*" class="custom-file-input" id="imgfoods" name="imgfoods" required> 
                    </div>
                </div>
            </div>
             <br/><button type="submit" name="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</body>

</html>