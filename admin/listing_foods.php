<?php
//include auth_session.php file on all user panel pages
include("../admin/functions.php");
include("header.php");

?>



<style>
	#site-header {
		position: relative;

	}

	.table-wrapper {

		margin: 30px auto;
		background: #fff;
		padding: 20px;
		box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
	}

	.table-title {
		padding-bottom: 10px;
		margin: 0 0 10px;
	}

	.table-title h2 {
		margin: 6px 0 0;
		font-size: 22px;
	}

	.table-title .add-new {
		float: right;
		height: 30px;
		font-weight: bold;
		font-size: 12px;
		text-shadow: none;
		min-width: 100px;
		border-radius: 50px;
		line-height: 13px;
	}

	.table-title .add-new i {
		margin-right: 4px;
	}

	table.table {
		table-layout: fixed;
	}

	table.table tr th,
	table.table tr td {
		border-color: #e9e9e9;
	}

	table.table th i {
		font-size: 13px;
		margin: 0 5px;
		cursor: pointer;
	}

	table.table th:last-child {
		width: 100px;
	}

	table.table td a {
		cursor: pointer;
		display: inline-block;
		margin: 0 5px;
		min-width: 24px;
	}

	table.table td a.add {
		color: #27C46B;
	}

	table.table td a.edit {
		color: #FFC107;
	}

	table.table td a.delete {
		color: #E34724;
	}

	table.table td i {
		font-size: 19px;
	}

	table.table td a.add i {
		font-size: 24px;
		margin-right: -1px;
		position: relative;
		top: 3px;
	}

	table.table .form-control {
		height: 32px;
		line-height: 32px;
		box-shadow: none;
		border-radius: 2px;
	}

	table.table .form-control.error {
		border-color: #f50000;
	}

	table.table td .add {
		display: none;
	}

	td.listmenu img {
		width: 100%;
		height: 100px;
	}


	/* Tab */
	.tab-block {
		width: 100%;
		height: auto;
		margin: 50px auto;
	}

	.tab-block .tab-mnu {
		display: block;
		list-style: none;
	}

	.tab-block .tab-mnu:after {
		content: "";
		display: table;
		clear: both;
	}

	.tab-block .tab-mnu li {
		box-sizing: border-box;
		float: left;
		background-color: #b2bbc0;
		color: white;
		width: 130px;
		text-align: center;
		padding-top: 13px;
		padding-bottom: 13px;
		cursor: pointer;
	}

	.tab-block .tab-mnu li:not(:last-child) {
		border-right: 1px solid #4c607c;
	}

	.tab-block .tab-mnu li:hover:not(.active) {
		background-color: #c0c7cb;
	}

	.tab-block .tab-mnu .active {
		background-color: #fff;
		color: #596165;
		border-bottom: 1px solid #eaecec;
		cursor: default;
	}

	.tab-block .tab-cont {
		box-sizing: border-box;
		border-top: 1px solid white;
		background-color: white;
		color: #292d2f;
	}

	.tab-block .tab-cont .tab-pane {
		padding: 20px 25px;
	}
</style>

<div class="container">
	<div class="table-responsive1">
		<div class="table-wrapper">
			<div class="table-title">
				<div class="row">
					<div class="col-sm-8">
						<h2>List all <b>Foods and drinks</b></h2>
					</div>
					<div class="col-sm-4">
						<a href="create_menu.php"><button type="button" class="btn btn-info add-new"><i class="fa fa-plus"></i> Add New</button></a>
						<a href="dashboard.php"><button type="button" class="btn btn-info add-new"><i class="fa fa-list-alt" aria-hidden="true"></i>
 Dashboard</button></a>
					</div>
				</div>
			</div>

			<!-- tab -->

			<div class="tab-block" id="tab-block">

				<ul class="tab-mnu">
					<li class="active">Lunch</li>
					<li>Dinner</li>
					<li>Drinks</li>
					<li>Desert</li>
					<li>list All</li>
				</ul>

				<div class="tab-cont">
					<!-- table lunch-->
					<div class="tab-pane">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>id</th>
									<th>picture</th>
									<th>Foods</th>
									<th>Price</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$sql = "SELECT * FROM foods WHERE typefoods = 'lunch' ORDER BY id DESC";
								$result = mysqli_query($con, $sql);
								$resultCheck = mysqli_num_rows($result);
								while ($row = $result->fetch_assoc()) { ?>
									<tr>
										<td><?php echo $row["id"] ?></td>
										<td class="listmenu"><img src="../admin/<?php echo $row['foodsimage'] ?>" alt=""></td>
										<td><?php echo $row["fname"] ?></td>

										<td><?php echo $row["pfood"] ?></td>
										<td>
											<a class="add" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>
 											<a class="edit" href="update-menu.php?id=<?php echo $row["id"]; ?>"><i class="material-icons">&#xE254;</i></a> 
											<a class="delete" href="delete-menu.php?id=<?php echo $row["id"]; ?>"><i class="material-icons">&#xE872;</i></a>
										</td>
									</tr>
								<?php } ?>


							</tbody>
						</table>
					</div>


					<div class="tab-pane">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>id</th>
									<th>picture</th>
									<th>Foods</th>
									<th>Price</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$sql = "SELECT * FROM foods WHERE typefoods = 'dinner' ORDER BY id DESC";
								$result = mysqli_query($con, $sql);
								$resultCheck = mysqli_num_rows($result);
								while ($row = $result->fetch_assoc()) { ?>
									<tr>
										<td><?php echo $row["id"] ?></td>
										<td class="listmenu"><img src="../admin/<?php echo $row['foodsimage'] ?>" alt=""></td>
										<td><?php echo $row["fname"] ?></td>

										<td><?php echo $row["pfood"] ?></td>
										<td>
											<a class="add" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>
											<a class="edit" href="update-menu.php?id=<?php echo $row["id"]; ?>"><i class="material-icons">&#xE254;</i></a> 
											<a class="delete" href="delete-menu.php?id=<?php echo $row["id"]; ?>"><i class="material-icons">&#xE872;</i></a>
										</td>
									</tr>
								<?php } ?>


							</tbody>
						</table>
					</div>
					<div class="tab-pane">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>id</th>
									<th>picture</th>
									<th>Foods</th>
									<th>Price</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$sql = "SELECT * FROM foods WHERE typefoods = 'drink' ORDER BY id DESC";
								$result = mysqli_query($con, $sql);
								$resultCheck = mysqli_num_rows($result);
								while ($row = $result->fetch_assoc()) { ?>
									<tr>
										<td><?php echo $row["id"] ?></td>
										<td class="listmenu"><img src="../admin/<?php echo $row['foodsimage'] ?>" alt=""></td>
										<td><?php echo $row["fname"] ?></td>

										<td><?php echo $row["pfood"] ?></td>
										<td>
											<a class="add" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>
											<a class="edit" href="update-menu.php?id=<?php echo $row["id"]; ?>"><i class="material-icons">&#xE254;</i></a> 
											<a class="delete" href="delete-menu.php?id=<?php echo $row["id"]; ?>"><i class="material-icons">&#xE872;</i></a>
										</td>
									</tr>
								<?php } ?>


							</tbody>
						</table>
					</div>
					<div class="tab-pane">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>id</th>
									<th>picture</th>
									<th>Foods</th>
									<th>Price</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$sql = "SELECT * FROM foods WHERE typefoods = 'dessert' ORDER BY id DESC";
								$result = mysqli_query($con, $sql);
								$resultCheck = mysqli_num_rows($result);
								while ($row = $result->fetch_assoc()) { ?>
									<tr>
										<td><?php echo $row["id"] ?></td>
										<td class="listmenu"><img src="../admin/<?php echo $row['foodsimage'] ?>" alt=""></td>
										<td><?php echo $row["fname"] ?></td>

										<td><?php echo $row["pfood"] ?></td>
										<td>
											<a class="add" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>
											<a class="edit" href="update-menu.php?id=<?php echo $row["id"]; ?>"><i class="material-icons">&#xE254;</i></a> 
											<a class="delete" href="delete-menu.php?id=<?php echo $row["id"]; ?>"><i class="material-icons">&#xE872;</i></a>
										</td>
									</tr>
								<?php } ?>


							</tbody>
						</table>
					</div>
					<div class="tab-pane">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>id</th>
									<th>picture</th>
									<th>Foods</th>
									<th>Price</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$sql = "SELECT * FROM foods ORDER BY id DESC";
								$result = mysqli_query($con, $sql);
								$resultCheck = mysqli_num_rows($result);
								while ($row = $result->fetch_assoc()) { ?>
									<tr>
										<td><?php echo $row["id"] ?></td>
										<td class="listmenu"><img src="../admin/<?php echo $row['foodsimage'] ?>" alt=""></td>
										<td><?php echo $row["fname"] ?></td>

										<td><?php echo $row["pfood"] ?></td>
										<td>
											<a class="add" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>
											<a class="edit" href="update-menu.php?id=<?php echo $row["id"]; ?>"><i class="material-icons">&#xE254;</i></a> 
											<a class="delete" href="delete-menu.php?id=<?php echo $row["id"]; ?>"><i class="material-icons">&#xE872;</i></a>
										</td>
									</tr>
								<?php } ?>


							</tbody>
						</table>
					</div>
				</div>

			</div>
			<!-- tab -->

		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
		var actions = $("table td:last-child").html();
		// Append table with add row form on add new button click
		$(".add-new").click(function() {
			$(this).attr("disabled", "disabled");
			var index = $("table tbody tr:last-child").index();
			var row = '<tr>' +
				'<td><input type="text" class="form-control" name="name" id="name"></td>' +
				'<td><input type="text" class="form-control" name="department" id="department"></td>' +
				'<td><input type="text" class="form-control" name="phone" id="phone"></td>' +
				'<td>' + actions + '</td>' +
				'</tr>';
			$("table").append(row);
			$("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
			$('[data-toggle="tooltip"]').tooltip();
		});
		// Add row on add button click
		$(document).on("click", ".add", function() {
			var empty = false;
			var input = $(this).parents("tr").find('input[type="text"]');
			input.each(function() {
				if (!$(this).val()) {
					$(this).addClass("error");
					empty = true;
				} else {
					$(this).removeClass("error");
				}
			});
			$(this).parents("tr").find(".error").first().focus();
			if (!empty) {
				input.each(function() {
					$(this).parent("td").html($(this).val());
				});
				$(this).parents("tr").find(".add, .edit").toggle();
				$(".add-new").removeAttr("disabled");
			}
		});
		// Edit row on edit button click
		$(document).on("click", ".edit", function() {
			$(this).parents("tr").find("td:not(:last-child)").each(function() {
				$(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
			});
			$(this).parents("tr").find(".add, .edit").toggle();
			$(".add-new").attr("disabled", "disabled");
		});
		// Delete row on delete button click
		$(document).on("click", ".delete", function() {
			$(this).parents("tr").remove();
			$(".add-new").removeAttr("disabled");
		});
	});

	// tab table
	$(document).ready(function() {

		var tabWrapper = $('#tab-block'),
			tabMnu = tabWrapper.find('.tab-mnu  li'),
			tabContent = tabWrapper.find('.tab-cont > .tab-pane');

		tabContent.not(':first-child').hide();

		tabMnu.each(function(i) {
			$(this).attr('data-tab', 'tab' + i);
		});
		tabContent.each(function(i) {
			$(this).attr('data-tab', 'tab' + i);
		});

		tabMnu.click(function() {
			var tabData = $(this).data('tab');
			tabWrapper.find(tabContent).hide();
			tabWrapper.find(tabContent).filter('[data-tab=' + tabData + ']').show();
		});

		$('.tab-mnu > li').click(function() {
			var before = $('.tab-mnu li.active');
			before.removeClass('active');
			$(this).addClass('active');
		});

	});
</script>
<?php include('footer.php')

?>