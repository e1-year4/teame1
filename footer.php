
		<!-- Footer -->
		<footer id="site-footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-4">
						<div class="bloc-cms">
							<a href="index.html" title="FoodLover" class="logo">
								<img src="assets/img/LOGO_Khfood.png" alt="" width="170">
							</a>
							<p>
								គេហទំព័រយើងខ្ញុំមានផ្ដល់ជូនសេវាកម្មរៀបចំអាហារក្នុងកម្មវិធីផ្សេងៗដូចជាពិធីជប់លៀងជប់លៀង
								កម្មវិធីអាពាហ៍ពិពាហ៍និងការជួបជុំបែបអន្តរជាតិ
								ប្រកបដោយអនាម័យនិងគុណភាពរសជាតិមិនអាចបំភ្លេចបាន។</p>
							<a class="toabout" href="about-us.html">អានបន្ត</a>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="open-hours">
							<span class="foot-title">សេវាកម្មបើកចាប់ពីម៉ោង</span>
							<p><span>ចន្ទ - សុក្រ : </span>................. 6 am - 12 pm</p>
							<p><span>សៅរ៍ : </span>.............. 6 am - 12 pm</p>
							<p><span>អាទិត្យ : </span>.................. 6 am - 3 pm</p>
						</div>
					</div>

					<div class="col-md-4 hidden-sm hidden-xs">
						<span class="foot-title">ទំនាក់ទំនងតាមប្រព័នឌីជីផល</span>
						<div class="socail">
							<ul>

								<li><a href="https://t.me/Khinsokha"><img src="assets/img/demo/logo_icons/512px-Telegram_logo.svg.png"
											alt=""></a></li>
								<li><a href="https://gitlab.com/e1-year4/teame1.git"><img src="assets/img/demo/logo_icons/5847f997cef1014c0b5e48c1.png"
											alt="gitlab"></a></li>
								<li><a href="https://web.facebook.com/profile.php?id=100009211666069"><img src="assets/img/demo/logo_icons/facebook-logo.png" alt="facebook-logo"></a></li>
								<li><a href="https://www.instagram.com/explore/locations/225235076/rupp-royal-university-of-phnom-penh/"><img src="assets/img/demo/logo_icons/Instagram_icon.png" alt=""></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-copyright">
				<p>
					Copyright 2022 © ក្រុមកិច្ចការស្រាវជ្រាវវេបផ្សាយថ្នាក់e1ឆ្នាំទី4 ។
				</p>
				<a href="">Top</a>
			</div>
		</footer>
		<!-- End Footer -->

	</div>
    <!-- Contribute JS Files -->
	<script type="text/javascript" src="assets/js/egprojets.lib.js"></script>
	<script type="text/javascript" src="assets/js/egprojets.custom.js"></script>
	<script type="text/javascript" src="assets/js/fontawesome.js"></script>

</body>

</html>