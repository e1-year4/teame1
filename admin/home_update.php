<?php
//include auth_session.php file on all user panel pages
include("../auth_session.php");
include('../functions.php');   // this function is use for insert data
$result = mysqli_query($con, "SELECT * FROM home");

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>List data home page</title>
    <link rel="stylesheet" href="../assets/admin_style.css" />
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
</head>

<body>
    <div class="form">
        <p>Hey, <?php echo $_SESSION['username']; ?>!</p>
        <p><a href="/teame1/admin/dashboard.php">Go to select pages</a></p>
        <p><a href="logout.php">Logout</a></p>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8">

                <?php
                if (isset($_SESSION['status'])) {
                ?>
                    <div role="alert">
                        <strong>Hey!</strong> <?php echo $_SESSION['status']; ?>
                        <button type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php
                    unset($_SESSION['status']);
                }
                ?>

                <div>
                    <div>
                        <h4>How to Update Date Values into Database in php</h4>
                    </div>
                    <!-- update form -->
                    <?php
                    if (mysqli_num_rows($result) > 0) {
                    ?>
                        <table>
                            <tr>

                                <td>home</td>
                            </tr>
                            <?php
                            $i = 0;
                            while ($row = mysqli_fetch_array($result)) {
                            ?>
                                <tr>
                                    <td><?php echo $row["name"]; ?></td>
                                    <td><a href="home_update_form.php?id=<?php echo $row["id"]; ?>">Update</a></td>
                                </tr>
                            <?php
                                $i++;
                            }
                            ?>
                        </table>
                    <?php
                    } else {
                        echo "No result found";
                    }
                    ?>



                </div>
            </div>
        </div>
     
     
</body>

</html>