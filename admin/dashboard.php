<?php
//include auth_session.php file on all user panel pages
include('functions.php');
include("auth_session.php");
include("header.php");
?>
<style>
    #site-header {
        position: relative;

    }

    .link-act {
        background-color: #dfdfdf !important;
        text-align: center;
        border-radius: 10px;
        padding: 9px 0px 30px !important;
    }

    .container {
        margin-bottom: 70px;
    }
</style>


<div class="form">
    <p>Hey, <?php echo $_SESSION['username']; ?>!</p>
    <p>You are in user dashboard page.</p>
    <p><a href="/teame1/" target="_blank">GO TO VIEW</a></p>
    <p><a href="logout.php">Logout</a></p>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-3">
            <span class="badge">1</span>
            <div class="link-act">
                <h3>Menu of foods</h3>
                <a class="btmlist" href="../admin/listing_foods.php">
                    <button type="button" class="btn btn-primary">Listing</button>
                </a>
                <a class="btncreate" href="../admin/create_menu.php">
                    <button type="button" class="btn btn-success">Create</button>
                </a>
            </div>
        </div>
        <!-- home page -->
        <div class="col-md-3">
            <span class="badge">1</span>
            <div class="link-act">
                <h3>Home page</h3>
                <a class="btmlist" href="../admin/listing_foods.php">
                    <button type="button" class="btn btn-primary">Listing</button>
                </a>

                <?php
                    $sql = "SELECT * FROM home ORDER BY id DESC LIMIT 1";
                    $result = mysqli_query($con, $sql);

                    while ($row = $result->fetch_assoc()) { ?>
                         <a class="btncreate" href="../admin/home_update_form.php?id=<?php echo $row["id"]; ?>">
                            <button type="button" class="btn btn-success">Create</button>
                        </a>
                    <?php } ?>



            </div>
        </div>
        <!-- About page -->
        <div class="col-md-3">
            <span class="badge">1</span>
            <div class="link-act">
                <h3>About page</h3>
                <a class="btmlist" href="../admin/listing_foods.php">
                    <button type="button" class="btn btn-primary">Listing</button>
                </a>
                <a class="btncreate" href="../admin/about.php?id=1">
                    <button type="button" class="btn btn-success">Create</button>
                </a>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card">
                <span class="badge">1</span>
                <div class="icon">
                    <i class="fas fa-chalkboard-teacher"></i>
                </div>
                <div class="text">
                    <?php
                    $sql = "SELECT * FROM home ORDER BY id DESC LIMIT 1";
                    $result = mysqli_query($con, $sql);

                    while ($row = $result->fetch_assoc()) { ?>
                        <a href="../admin/home_update_form.php?id=<?php echo $row["id"]; ?>">Update Home Page</a>
                    <?php } ?>

                </div>
            </div>
        </div>


    </div>
</div>

<?php include('footer.php'); ?>