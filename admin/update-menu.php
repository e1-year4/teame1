<?php
include("../admin/functions.php");
require_once 'menu_operation.php';
include('db.php');
include_once 'header.php';
  
if(count($_POST)>0) {
    mysqli_query($con,"UPDATE foods set 
    id='" . $_POST['id'] . "',
    fname='" . $_POST['fname'] . "',
    pfood='" . $_POST['pfood'] . "', 
    typefoods='" . $_POST['typefoods'] . "',
    WHERE id='" . $_POST['id'] . "'");
    $message = "Record Modified Successfully";
    }
    $result = mysqli_query($con,"SELECT * FROM foods WHERE id='" . $_GET['id'] . "'");
    $row= mysqli_fetch_array($result);
?>
<style>
    #site-header {
        position: relative;
    }
</style>





<!-- update menu -->
<div class="container">

    <form name="frmMenu" method="POST" action="">
        <div><?php if (isset($message)) {
                    echo $message;
                } 
               
                ?>
        </div>
        <input type="text" name="id" value="<?php echo $row['id']; ?>">
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <label for="fname">Name Of Food</label>
                    <input type="text" name="fname" id="namefood" class="form-control" value="<?php echo $row['fname']; ?>" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pfood">Price</label>
                    <input type="munber" step="any" name="pfood" id="pfood" class="form-control" value="<?php echo $row['pfood']; ?>" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typefoods" id="lunch" value="lunch" required>
                            <label class="form-check-label" for="lunch">
                                អាហារថ្ងៃត្រង់
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typefoods" id="dinner" value="dinner" required>
                            <label class="form-check-label" for="dinner">
                                អាហារពេលល្ងាច
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typefoods" id="drink" value="drink" required>
                            <label class="form-check-label" for="drink">
                                ភេសជ្ជៈ
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typefoods" id="dessert" value="dessert" required>
                            <label class="form-check-label" for="dessert">
                                បង្អែមខ្មែរនិងបរទេស
                            </label>
                        </div>

                    </div>

                </div>
            </div>

            <div class="col-md-6">
                <div class="custom-file">
                <img src="admin/<?php echo $foodsimage; ?>" alt="image foods">
                    <label class="custom-file-lable" for="imgfoods">Foods picture</label>
                    
                    <input type="file" accept="image/*" class="custom-file-input" id="imgfoods" name="imgfoods" value="<?php echo $row['foodsimage']; ?>">
                </div>
            </div>
        </div>
        <br/><button type="submit" name="submit" class="btn btn-primary">Update</button>
 </div>
   
</form>